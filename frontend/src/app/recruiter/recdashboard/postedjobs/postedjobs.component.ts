import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';
import { Jobs } from 'src/app/models';
@Component({
  selector: 'app-postedjobs',
  templateUrl: './postedjobs.component.html',
  styleUrls: ['./postedjobs.component.css']
})
export class PostedjobsComponent implements OnInit {

  headers=['Role',
'Description',
'Type',
'Domain',
'Compensation',
'Open For',
'Location',
'Company',
]

  posted:any =[];
  nojobs:any;
  totaljobs:any;
  errormsg:any;
  successmsg:boolean=false;
  constructor(private router:Router,private activeroute:ActivatedRoute,private authService:AuthService) { }

  ngOnInit() {
    this.postedjobs();
  }
  postedjobs()
  {
    this.authService.getpostedjobs().subscribe(
      (response:any)=>{
        if(response.status && response.status===1)
        this.nojobs=response.message;
      else{
        this.posted=response;
        this.successmsg=true;
        //console.log(this.applied);
      }
    },(error)=>{
      this.errormsg=error;
    }
    )
  }
}