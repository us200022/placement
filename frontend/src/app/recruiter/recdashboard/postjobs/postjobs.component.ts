import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';
import { Jobs } from 'src/app/models';
@Component({
  selector: 'app-postjobs',
  templateUrl: './postjobs.component.html',
  styleUrls: ['./postjobs.component.css']
})
export class PostjobsComponent implements OnInit {

  jobs: Jobs[]=[];
  postjobForm: FormGroup;
  postedMsg:any;
  alreadyposted:any;
  errormsg:any;
  //companyname:any;
  //companyId:any;
  constructor(private router: Router, private authService: AuthService, private fb: FormBuilder) { }

  ngOnInit() {
   // this.companyname=this.authService.getpayload().companyName;
    //this.companyId=this.authService.getpayload().id;
    this.postjobForm = this.fb.group({
      //companyId: new FormControl(this.companyId),
      jobRole: ['', Validators.required],
      jobDescription: ['', Validators.required],
      jobType: ['', Validators.required],
     jobDomain: ['', Validators.required],
      jobCompensation: ['', Validators.required],
      jobLocation: ['', Validators.required],
      jobOpenFor: ['', Validators.required],
      company: ['', Validators.required]
    })
  }
  logoutRecruiter() {
    this.authService.logout();
    this.router.navigate(['login/rec_login'])
  }
  postajob() {
    console.log(this.postjobForm.value);
    this.authService.postjob(JSON.stringify(this.postjobForm.value)).subscribe(
      (response:any)=>{
        if(response.status && response.status==1)
        {
          this.postedMsg=response.message;
          setTimeout(()=>{
            this.postedMsg='';
           // this.router.navigate(['rdashboard/postedjobs']);
          },2000);
        }else{
          this.alreadyposted=response.message;
          setTimeout(()=>{
            this.alreadyposted='';
          },2000);
        }
      },(error)=>{
        this.errormsg="Internal Server Error";
      }
    )
  }

}