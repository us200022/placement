import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/_services/auth.service';
import { Subscription } from 'rxjs';
import {   Jobs } from 'src/app/models';

@Component({
  selector: 'app-appliedjobs',
  templateUrl: './appliedjobs.component.html',
  styleUrls: ['./appliedjobs.component.css']
})
export class AppliedjobsComponent implements OnInit {

  applied:any =[];
nojobs:any;
errormsg:any;
successmsg:boolean;
headers=['Role',
'Description',
'Type',
'Domain',
'Compensation',
'Open For',
'Location',
'Company',
]
  constructor(private router:Router,private authService:AuthService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.appliedjobs();
  }
  appliedjobs()
  {
    this.authService.getappliedjobs().subscribe(
      (response:any)=>{
        if(response.status && response.status===1)
        this.nojobs=response.message;
      else{
        this.applied=response;
        this.successmsg=true;
        //console.log(this.applied);
      }
    },(error)=>{
      this.errormsg=error;
    }
    )
  }

}

