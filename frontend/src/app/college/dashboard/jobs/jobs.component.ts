import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/_services/auth.service';
import { Subscription } from 'rxjs';
import {   Jobs } from 'src/app/models';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  jobs: Jobs[]=[];
waitforjobs:any;
appliedmessage:any;
alreadyapplied:any;
errormessage:any;
totaljobs:any;

headers=['Role',
'Description',
'Type',
'Domain',
'Compensation',
'Open For',
'Location',
'Company',
]

public routeSub: Subscription;
public jobSub:Subscription;
  constructor(private router:Router,private authService:AuthService,private route: ActivatedRoute) { }
  ngOnInit() {
  this.routeSub=this.route.params.subscribe((response:any)=>this.getJobs());;
  }

  /*getjobs()
  {
    this.authService.getjobList().subscribe(data => {
      this.jobs = data;
    },
  /*this.jobSub=  this.authService.getjobList().subscribe(
      (jobList)=>
    {
      
       this.jobs=jobList;
       console.log(jobList);
       console.log(this.jobs);
      
    },
    (error)=>{
      console.log(error.msg);
    }
    );
  }*/
  getJobs() {
    this.authService.getjobList().subscribe(data => {
      this.jobs = data;
    });
  }
  apply(jobapply:any)
  {
    console.log(jobapply);
    this.authService.applyjob(jobapply).subscribe(
      (response:any)=>{
        if(response.status && response.status==1){
          //console.log(response);
         // 
         this.appliedmessage=response.message;
         setTimeout(()=>{
          this.appliedmessage='';
          this.getJobs();
         },2000) ;
        }else{
          this.alreadyapplied=response.message; 
          setTimeout(()=>{
            this.alreadyapplied='';
            //this.getjobs();
           },1000);
        }
        
      },(err)=>{
        this.errormessage=err.message;
      }
    );
    
  }
}