import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { Router } from '@angular/router';
import {first} from "rxjs/operators";
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  profileinfo: any;
  ProfileForm: FormGroup;
  updated: any;
  userdata:any={};
  constructor(private router: Router, private fb: FormBuilder, private authService: AuthService) { }

  ngOnInit() {
    this.getprofile();
    this.ProfileForm = this.fb.group({
     // _id: this.authService.getpayload().id,
     collegeName: [''],
      password: ['', Validators.compose([Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})"), Validators.minLength(8)])],
      collegeEmail: ['', Validators.compose([Validators.email])],
     collegeNirfRanking: [''],
    collegeAicteAffiliation: [''],
      collegeWebsite: [''],
      collegeLocation:['']
    });
  }
  getprofile() {
    this.authService.getprofile().subscribe(
      (response: any) => {
        this.profileinfo = response;
        this.userdata=response;
        console.log(this.userdata);
        this.ProfileForm.patchValue({
         // _id: this.authService.getpayload().id,
          collegeName: this.userdata.collegeName,
         collegeEmail: this.userdata.collegeEmail,
          collegeNirfRanking: this.userdata.collegeNirfRanking,
          collegeWebsite: this.userdata.collegeWebsite,
          collegeLocation: this.userdata.collegeLocation,
          collegeAicteAffiliation: this.userdata.collegeAicteAffiliation,
         
        });
       /* this.ProfileForm.setValue({
          password: this.userdata.password
        });*/

      }, (error) => {
        console.log("Server Error");
      }
    )
  }

  updateprofile() {
   
    this.authService.Empupdateprofile(JSON.stringify(this.ProfileForm.value)).subscribe((response) => {

      this.updated = response;
     // localStorage.setItem('currentemployee',this.EmpProfileForm.value.username);
      //this.EmpProfileForm.setValue(response);
      this.router.navigate(['editprofile'])
      console.log(this.ProfileForm.value);
    },
      (error) => {
        console.log(error);
      })
  }
}